FROM node:8-alpine
ADD . src/
WORKDIR /src
RUN npm i -gq typescript && npm i -q && npm run build -q
# RUN npm i
# RUN npm run build
CMD npm start -q